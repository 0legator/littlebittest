﻿using System;
using InternalAssets.Scripts.UI;

namespace InternalAssets.Scripts.Services
{
    public class SystemUIService : ISystemUIService
    {
        private readonly IUIFactory _uiFactory;

        private IElementUI _gameUI;
        private IElementInteractiveUI _finishUI;
        public SystemUIService(IUIFactory uiFactory)
        {
            _uiFactory = uiFactory;
        }

        public void ShowGameUI(ILevelInfoService levelInfoService, IZoneSystemService zoneSystemService)
        {
            _gameUI = _uiFactory.CreateGameUI();
            _gameUI.Enable();
            zoneSystemService.OnUpdateZone += (value, value1)=>
            {
                _gameUI.ReturnMonoBehaviour().GetComponent<GameUI>()
                    .UpdateProgressBar(zoneSystemService);
            };
        }
        
        
        
        public void ShowFinishUI(Action OnClick)
        {
            _gameUI.Disable();
            _finishUI = _uiFactory.CreateFinishUI();
            _finishUI.OnClick += OnClick;
            _finishUI.Enable();
        }
    }
}