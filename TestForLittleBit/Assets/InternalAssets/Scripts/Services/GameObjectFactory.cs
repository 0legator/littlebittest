﻿using System;
using System.Collections.Generic;
using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Interfaces;
using InternalAssets.Scripts.LevelLogic;
using InternalAssets.Scripts.ScriptableObjects;
using UnityEngine;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace InternalAssets.Scripts.Services
{
    public class GameObjectFactory : IGameObjectFactory
    {
        private List<Zone> _zones;
        private LevelSettingsSO _levelSettings;
        private readonly ILevelInfoService _levelInfoService;

        public GameObjectFactory(IAssetProvider assetProvider, ILevelInfoService levelInfoService,
            List<Zone> zones)
        {
            _levelInfoService = levelInfoService;
            _zones = zones;
        }

        public List<GameObject> CreateCharacter()
        {
            List<GameObject> characters = new List<GameObject>();

            _levelSettings = _levelInfoService.LoadLevel();
            
            foreach (var data in _levelSettings.CharacterDatas)
            {
                for (int i = 0; i < data.Nums; i++)
                {
                    characters.Add(GeneratePeople(data.Character, data, GetRandomPositionByBound(_zones.Random().Bounds)));
                }
            }
            
            return characters;
        }
        

        public IInputSystem CreateInputSystem()
        {
            return Object.Instantiate(Resources.Load<InputSystemService>(AssetPath.InputSystemPath));
        }

        public ICameraSystemService CreateCameraSystem()
        {
            var cameraSystemService = Object.Instantiate(Resources.Load<CameraSystemService>(AssetPath.CameraSystemPath));

            return cameraSystemService;
        }

        private GameObject GeneratePeople(Character characterPrefab, CharacterData data, Vector3 position)
        {
            Character character = Object.Instantiate(characterPrefab, position + Vector3.up, Quaternion.identity);
            character.Init(data.Type, data.MovementType);
            return character.gameObject;
        }

        private Vector3 GetRandomPositionByBound(Bounds bounds)
        {
            float GetRandomPoint(float min, float max) => Random.Range(min, max);
            return new Vector3(GetRandomPoint(bounds.min.x, bounds.max.x), 0, GetRandomPoint(bounds.min.z, bounds.max.z));
        }
    }
}