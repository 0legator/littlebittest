﻿using System.Collections.Generic;
using InternalAssets.Scripts.LevelLogic;

namespace InternalAssets.Scripts.Services
{
    public interface IFindZoneService : IService
    {
        public List<Zone> GetZones();
    }
}