﻿namespace InternalAssets.Scripts.Services
{
    public interface ISelectableService : IService
    {
        public ISelectable GetSelectable();
    }
}