﻿using InternalAssets.Scripts.ScriptableObjects;

namespace InternalAssets.Scripts.Services
{
    public interface ILevelInfoService : IService
    {
        public LevelSettingsSO LoadLevel();
    }
}