﻿using System.Collections.Generic;
using System.Linq;
using InternalAssets.Scripts.LevelLogic;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public class FindZoneService : IFindZoneService
    {
        public List<Zone> GetZones()
        {
            return Object.FindObjectsOfType<Zone>().ToList();
        }
        
    }
}