﻿using System;
using System.Collections.Generic;
using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.Interfaces;
using InternalAssets.Scripts.ScriptableObjects;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public class InputSystemService : MonoBehaviour, IInputSystem
    {
        public event Action<IMovable> onClickCharacter; 
        public event Action<Vector3> onMoveCharacter; 
        public event Action<ISelectable> onSelect;
        public event Action<bool> onRaycastHit;
        public event Action<Vector2> onMouseDown;

        private List<IMovable> _characters = new List<IMovable>();
        private IMovable currentClickCharacter;
        private LayerMask _layerMask;
        private Camera _camera;
        private bool _isHit;
        private Vector3 _lastHit;

        private enum MouseState
        {
            none,
            down,
            drag,
            up
        }
        private MouseState _mouseState = MouseState.up;

        public Vector2 MousePosition => Input.mousePosition;

        public void Construct(List<GameObject> characters, Camera camera)
        {
            foreach (var character in characters)
            {
                if(character.TryGetComponent(out IMovable movable))
                {
                    _characters.Add(movable);
                }
            }

            _camera = camera;
            _layerMask = Resources.Load<LayerMaskSO>(AssetPath.LayerMaskPath).LayerMask;
            
            onClickCharacter += OnClickCharacter;
            onMoveCharacter += OnMoveAt;
        }

        private void OnMoveAt(Vector3 at)
        {
            currentClickCharacter?.MoveTo(at);
        }

        private void OnClickCharacter(IMovable iCharacter)
        {
            if(currentClickCharacter != iCharacter)
                currentClickCharacter = iCharacter;
            else
            {
                currentClickCharacter = null;
            }
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                _mouseState = MouseState.down;
            }

            else if (Input.GetMouseButtonUp(0))
            {
                _mouseState = MouseState.up;
            }
            
            else if (Input.GetMouseButton(0))
            {
                _mouseState = MouseState.drag;
            }
            else
            {
                _mouseState = MouseState.none;
            }
            CheckState(_mouseState);
        }

        private void CheckState(MouseState state)
        {
            if (state == MouseState.down)
            {
                onMouseDown?.Invoke(Input.mousePosition);
                Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, 100f, _layerMask, QueryTriggerInteraction.Ignore))
                {
                    if (hit.collider != null)
                    {
                        if (hit.collider.TryGetComponent(out ISelectable selectable))
                        {
                            onSelect?.Invoke(selectable);
                        }
                        if (hit.collider.TryGetComponent(out IMovable character))
                        {
                            onClickCharacter?.Invoke(character);
                        }
                        else if(currentClickCharacter is IMovableToPoint)
                        {
                            onMoveCharacter?.Invoke(hit.point);
                        }
                    }
                    _isHit = true;

                    _lastHit = GetWorldPos();
                }
                else
                {
                    _isHit = false;
                }
            }
            else if (state == MouseState.drag)
            {
                onRaycastHit?.Invoke(_isHit);
            }
            else if (state == MouseState.up)
            {
                if (currentClickCharacter is IMovableSwipe)
                {
                    currentClickCharacter.MoveTo(GetWorldPos() - _lastHit);
                }
                _isHit = false;
            }
        }

        private Vector3 GetWorldPos()
        {
            Plane plane = new Plane(Vector3.up, Vector3.down);
            float enter;
            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);
            if (plane.Raycast(ray, out enter))
            {
            }

            return ray.GetPoint(enter);
        }
        
        public void DisableService()
        {
            Destroy(gameObject);
        }
    }
}