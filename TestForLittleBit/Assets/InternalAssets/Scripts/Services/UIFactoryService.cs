﻿using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.UI;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public class UIFactoryService : IUIFactory
    {
        private readonly IAssetProvider _assetProvider;

        public UIFactoryService(IAssetProvider assetProvider)
        {
            _assetProvider = assetProvider;
        }
        
        public IElementUI CreateGameUI()
        {
            return _assetProvider.Instantiate(AssetPath.GameUIPath).GetComponent<IElementUI>();
        }

        public IElementInteractiveUI CreateFinishUI()
        { 
            return _assetProvider.Instantiate(AssetPath.WinUIPath).GetComponent<IElementInteractiveUI>();
        }
    }
}