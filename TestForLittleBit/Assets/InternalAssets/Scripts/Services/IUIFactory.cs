﻿using InternalAssets.Scripts.UI;

namespace InternalAssets.Scripts.Services
{
    public interface IUIFactory : IService
    {
        public IElementUI CreateGameUI();
        public IElementInteractiveUI CreateFinishUI();
    }
}