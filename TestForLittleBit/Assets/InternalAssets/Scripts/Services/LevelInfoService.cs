﻿using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.ScriptableObjects;

namespace InternalAssets.Scripts.Services
{
    public class LevelInfoService : ILevelInfoService
    {
        private readonly IAssetProvider _assetProvider;

        public LevelInfoService(IAssetProvider assetProvider)
        {
            _assetProvider = assetProvider;
        }
        
        public LevelSettingsSO LoadLevel()
        {
            if (PlayerPrefsController.Level >=
                _assetProvider.LoadPrefab<LevelsSO>(AssetPath.LevelsSettingsPath).Levels.Count)
                PlayerPrefsController.Level = 0;
            
            return _assetProvider.LoadPrefab<LevelsSO>(AssetPath.LevelsSettingsPath).Levels[PlayerPrefsController.Level];
        }
    }
}