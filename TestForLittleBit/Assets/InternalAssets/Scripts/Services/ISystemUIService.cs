﻿using System;

namespace InternalAssets.Scripts.Services
{
    public interface ISystemUIService : IService
    {
        public void ShowGameUI(ILevelInfoService levelInfoService, IZoneSystemService zoneSystemService);
        public void ShowFinishUI(Action OnClick);
    }
}