﻿namespace InternalAssets.Scripts.Services
{
    public interface ISelectable
    {
        public void Select();
        public void DeSelect();
    }
}