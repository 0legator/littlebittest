﻿using System.Collections.Generic;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public class SelectableService : ISelectableService
    {
        private readonly List<ISelectable> _selectables = new List<ISelectable>();
        private ISelectable _currentSelected;
        public SelectableService(List<GameObject> selectables, IInputSystem inputSystem)
        {
            foreach (var selectable in selectables)
            {
                if (selectable.TryGetComponent(out ISelectable temp))
                {
                    _selectables.Add(temp);
                }
            }

            inputSystem.onSelect += SelectMovableCharacter;
        }

        private void SelectMovableCharacter(ISelectable selectable)
        {
            Debug.Log("SelectMovableCharacter");
            if (_currentSelected == selectable)
            {
                _currentSelected?.DeSelect();
                _currentSelected = null;
            }
            else
            {
                _currentSelected?.DeSelect();
                _currentSelected = selectable;
                _currentSelected?.Select();
            }
        }

        public ISelectable GetSelectable()
        {
            return _currentSelected;
        }
    }
}