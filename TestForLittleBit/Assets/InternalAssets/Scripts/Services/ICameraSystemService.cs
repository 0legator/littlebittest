﻿using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public interface ICameraSystemService : IService
    {
        public Camera GetMainCamera();
        public void Construct(ISelectableService selectableService, IInputSystem inputSystem);
    }
}