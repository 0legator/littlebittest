﻿using System;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.LevelLogic;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public interface IZoneSystemService : IService
    {
        public event Action<ColorChanger, bool> OnUpdateZone;
        public void OnEnter(Collider collider, Zone zone); 
        public void OnExit(Collider collider, Zone zone);
        public float ReturnProgress();
    }
}