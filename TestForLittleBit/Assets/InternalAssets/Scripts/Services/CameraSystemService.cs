﻿using System;
using Cinemachine;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public class CameraSystemService : MonoBehaviour, ICameraSystemService
    {
        [SerializeField] private Camera mainCamera;
        [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera;
        private ICinemachineCamera _activeCinemachineCamera;
        private Vector2 _lastMousePos = Vector2.zero;
        private CinemachineOrbitalTransposer _cinemachineOrbitalTransposer;
        private ISelectableService _selectableService;
        private IInputSystem _inputSystem;
        private void Start()
        {
            _cinemachineVirtualCamera.Follow = FindObjectOfType<CinemachineTargetGroup>().transform;
            _cinemachineVirtualCamera.LookAt = FindObjectOfType<CinemachineTargetGroup>().transform;
            _cinemachineOrbitalTransposer = _cinemachineVirtualCamera.GetCinemachineComponent<CinemachineOrbitalTransposer>();
        }

        public Camera GetMainCamera()
        {
            return mainCamera;
        }

        public void Construct(ISelectableService selectableService, IInputSystem inputSystem)
        {
            _selectableService = selectableService;
            _inputSystem = inputSystem;
            _inputSystem.onRaycastHit += OnRaycastHit;
            _inputSystem.onMouseDown += OnMouseDown;
        }

        private void OnMouseDown(Vector2 pos)
        {
            _lastMousePos = pos;
        }

        private void OnRaycastHit(bool isHit)
        {
            if (isHit && _selectableService.GetSelectable() != null) return;
           
            _cinemachineOrbitalTransposer.m_XAxis.Value += (_inputSystem.MousePosition.x - _lastMousePos.x) / 2f;
            _lastMousePos = _inputSystem.MousePosition;
        }
    }
}