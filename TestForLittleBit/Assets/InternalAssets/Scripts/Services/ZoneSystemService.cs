﻿using System;
using System.Collections.Generic;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.LevelLogic;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public class ZoneSystemService : IZoneSystemService
    {
        private readonly ILevelInfoService _levelInfoService;
        private readonly Action _onComplete;
        private Dictionary<PeopleType, int> _zonesCounter = new Dictionary<PeopleType, int>();

        public event Action<ColorChanger, bool> OnUpdateZone;

        public ZoneSystemService(List<Zone> zones, ILevelInfoService levelInfoService, Action onComplete)
        {
            _levelInfoService = levelInfoService;
            _onComplete = onComplete;
            foreach (var zone in zones)
            {
                _zonesCounter.Add(zone.ZoneType, 0);
            }
        }

        public void OnEnter(Collider collider, Zone zone)
        {
            if (collider.TryGetComponent(out ICharacterTypeOwner characterTypeOwner))
            {
                if (characterTypeOwner.PeopleType == zone.ZoneType)
                { 
                    _zonesCounter[zone.ZoneType]++;
                    OnUpdateZone?.Invoke(collider.GetComponent<ColorChanger>(), true);
                    CheckFull();
                }
            }
        }

        public void OnExit(Collider collider, Zone zone)
        {
            if (collider.TryGetComponent(out ICharacterTypeOwner characterTypeOwner))
            {
                if (characterTypeOwner.PeopleType == zone.ZoneType)
                {
                    _zonesCounter[zone.ZoneType]--;
                    OnUpdateZone?.Invoke(collider.GetComponent<ColorChanger>(), false);
                    CheckFull();
                }
            }
        }

        private void CheckFull()
        {
            foreach (var characterData in _levelInfoService.LoadLevel().CharacterDatas)
            {
                if (_zonesCounter[characterData.Type] != characterData.Nums)
                {
                    return;
                }
            }
            
            _onComplete?.Invoke();
        }

        public float ReturnProgress()
        {
            int characters = 0;
            int correctCharacters = 0;
            foreach (var characterData in _levelInfoService.LoadLevel().CharacterDatas)
            {
                characters += characterData.Nums;
                correctCharacters += _zonesCounter[characterData.Type];
            }

            return (float)correctCharacters / characters;
        }
    }
}