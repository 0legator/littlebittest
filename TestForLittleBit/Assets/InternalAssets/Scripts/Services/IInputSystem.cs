﻿using System;
using System.Collections.Generic;
using InternalAssets.Scripts.Interfaces;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public interface IInputSystem : IService
    {
        public Vector2 MousePosition { get; }
        public void Construct(List<GameObject> characters, Camera camera);
        public event Action<IMovable> onClickCharacter; 
        public event Action<Vector3> onMoveCharacter;
        public event Action<ISelectable> onSelect;
        public event Action<bool> onRaycastHit;
        public event Action<Vector2> onMouseDown;
        public void DisableService();
    }
}