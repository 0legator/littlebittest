﻿using System.Collections.Generic;
using UnityEngine;

namespace InternalAssets.Scripts.Services
{
    public interface IGameObjectFactory : IService
    {
        public List<GameObject> CreateCharacter();
        public IInputSystem CreateInputSystem();
        public ICameraSystemService CreateCameraSystem();
    }
}