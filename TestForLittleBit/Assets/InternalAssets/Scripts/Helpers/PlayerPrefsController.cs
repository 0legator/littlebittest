﻿using UnityEngine;

namespace InternalAssets.Scripts.Helpers
{
    public static class PlayerPrefsController
    {
        public static int Level
        {
            get => PlayerPrefs.GetInt("Level", 0);
            set
            {
                PlayerPrefs.SetInt("Level", value);
                LevelForAnalytics++;
            }
        }

        public static int LevelForAnalytics
        {
            get => PlayerPrefs.GetInt("LevelForAnalytics", 1);
            set => PlayerPrefs.SetInt("LevelForAnalytics", value);
        }
    }
}