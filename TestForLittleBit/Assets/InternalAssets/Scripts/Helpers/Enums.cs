﻿namespace InternalAssets.Scripts.Helpers
{
     public enum PeopleType
     {
         None = 0,
         Blue = 1,
         Red = 2,
         Green = 3
     }

     public enum MovementType
     {
         None = 0,
         NavMesh = 1,
         Swipe = 2
     }
}