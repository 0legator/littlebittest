﻿using System.Collections.Generic;

namespace InternalAssets.Scripts.Helpers
{
    public static class ExternalList
    {
        public static T Random<T>(this List<T> list)
        {
            if (list.Count == 0)
            {
                return default;
            }
            return list[UnityEngine.Random.Range(0, list.Count)];
        }
    }
}