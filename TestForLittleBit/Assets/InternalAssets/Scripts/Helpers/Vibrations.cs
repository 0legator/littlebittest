﻿using MoreMountains.NiceVibrations;
using UnityEngine;

public static class Vibrations
{
    private static float _lastTime;

    public static void Play(HapticTypes type, float time = 0.5f)
    {
        if (Cooldown(time)) return;
        _lastTime = Time.time;
        MMVibrationManager.Haptic(type);
    }
	
    public static void PlayContinious(float duration)
    {
        _lastTime = Time.time + duration;
        MMVibrationManager.ContinuousHaptic(0.5f, 0.5f, duration, HapticTypes.MediumImpact);
    }

    private static bool Cooldown(float time)
    {
        return _lastTime > Time.time || Time.time - _lastTime <= time;
    }
}