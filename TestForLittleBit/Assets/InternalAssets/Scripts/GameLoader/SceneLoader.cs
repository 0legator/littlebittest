﻿using System;
using System.Collections;
using InternalAssets.Scripts.Helpers;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InternalAssets.Scripts.GameLoader
{
    public class SceneLoader
    {
        private readonly ICoroutineRunner _coroutineRunner;

        public SceneLoader(ICoroutineRunner coroutineRunner)
        {
            _coroutineRunner = coroutineRunner;
        }

        public void LoadScene(string scene, Action onComplete, Action<float> onProgressUpdate)
        {
            _coroutineRunner.StartCoroutine(LoadingScene(scene, onComplete, onProgressUpdate));
        }
        
        private IEnumerator LoadingScene(string scene, Action onComplete, Action<float> onProgressUpdate)
        {
            // if (SceneManager.GetActiveScene().name == scene)
            // {
            //     onComplete?.Invoke();
            //     yield break;
            // }
            
            AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single);

            while (!asyncLoad.isDone)
            {
                onProgressUpdate?.Invoke(Mathf.Clamp(asyncLoad.progress / 0.9f, 0, 1));
                yield return null;
            }

            onComplete?.Invoke();
        }
    }
}