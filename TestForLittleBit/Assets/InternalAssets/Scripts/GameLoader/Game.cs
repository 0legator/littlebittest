﻿using InternalAssets.Scripts.Helpers;

namespace InternalAssets.Scripts.GameLoader
{
    public class Game
    {
        public GameStateMachine gameStateMachine;
        
        public Game(ICoroutineRunner coroutineRunner)
        {
            gameStateMachine = new GameStateMachine(new SceneLoader(coroutineRunner));
        }
    }
}