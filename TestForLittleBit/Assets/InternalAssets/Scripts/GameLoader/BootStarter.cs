﻿using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Services;
using InternalAssets.Scripts.States;
using UnityEngine;

namespace InternalAssets.Scripts.GameLoader
{
    public class BootStarter : MonoBehaviour, ICoroutineRunner, IService
    {
        private Game _game;

        private void Awake()
        {
            if (AllServices.Container.Single<BootStarter>() == null)
            {
                AllServices.Container.RegisterSingle<BootStarter>(this);
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            _game = new Game(this);
            _game.gameStateMachine.Enter<BootStarterState>();
            DontDestroyOnLoad(this);
        }
    }
}