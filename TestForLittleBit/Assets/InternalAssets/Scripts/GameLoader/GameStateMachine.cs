﻿using System;
using System.Collections.Generic;
using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Services;
using InternalAssets.Scripts.States;

namespace InternalAssets.Scripts.GameLoader
{
    public class GameStateMachine
    {
        private readonly Dictionary<Type, IState> _states;
        private IState _currentState;
        private readonly AllServices _allServices;
        public GameStateMachine(SceneLoader sceneLoader)
        {
            _allServices = AllServices.Container;
            _states = new Dictionary<Type, IState>()
            {
                [typeof(BootStarterState)] = new BootStarterState(this, sceneLoader, _allServices),
                [typeof(LoadLevelCharacterState)] = new LoadLevelCharacterState(this, sceneLoader, _allServices),
                [typeof(GameLoopLoadLevelCharacter)] = new GameLoopLoadLevelCharacter(this, sceneLoader, _allServices),
                [typeof(EndLevelState)] = new EndLevelState(this, _allServices)
            };
        }

        public void Enter<TState>() where TState : IState
        {
            _currentState?.Exit();
            _currentState = _states[typeof(TState)];
            _currentState.Enter();
        }
    }
}