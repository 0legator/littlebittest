﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace InternalAssets.Scripts.GameLoader
{
    public class BootGame : MonoBehaviour
    {
        private BootStarter _bootStarter;
        private void Awake()
        {
            _bootStarter = FindObjectOfType<BootStarter>();
            if (_bootStarter == null)
            {
                SceneManager.LoadScene(0, LoadSceneMode.Single);
            }
        }
    }
}