﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Utils
{
    public class WaitStart : MonoBehaviour
    {
        [SerializeField] private float timeWait = 1;
        [SerializeField] private UnityEvent onComplete;

        private Coroutine _coroutine;

        private void OnEnable()
        {
            _coroutine = Wait.ForSeconds(timeWait, () => onComplete?.Invoke());
        }

        private void OnDisable()
        {
            if (_coroutine != null)
                Wait.Stop(_coroutine);
        }

        private void OnDestroy()
        {
            if (_coroutine != null)
                Wait.Stop(_coroutine);
        }
    }
}