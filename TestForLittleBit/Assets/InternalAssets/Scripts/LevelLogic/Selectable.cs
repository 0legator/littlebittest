﻿using System;
using EPOOutline;
using InternalAssets.Scripts.Services;
using UnityEngine;

namespace InternalAssets.Scripts.LevelLogic
{
    [RequireComponent(typeof(Outlinable))]
    public class Selectable : MonoBehaviour, ISelectable
    {
        private Outlinable _outlinable;

        private void Awake()
        {
            _outlinable = GetComponent<Outlinable>();
        }

        public void Select()
        {
            _outlinable.enabled = true;
        }

        public void DeSelect()
        {
            _outlinable.enabled = false;
        }
    }
}