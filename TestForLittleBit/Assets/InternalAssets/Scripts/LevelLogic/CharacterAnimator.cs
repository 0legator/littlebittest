﻿using System;
using InternalAssets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.AI;

namespace InternalAssets.Scripts.LevelLogic
{
    public class CharacterAnimator : MonoBehaviour
    {
        private Animator _animator;
        private IMovable _movable;

        private readonly int _hashSpeed = Animator.StringToHash("Speed");
        
        private void Awake()
        {
            _animator = GetComponentInChildren<Animator>();
            _movable = GetComponent<IMovable>();
        }

        private void Update()
        {
            _animator.SetFloat(_hashSpeed, _movable.Velocity.magnitude);
        }
    }
}