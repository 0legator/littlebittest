﻿using System;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Interfaces;
using InternalAssets.Scripts.Services;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.AI;

namespace InternalAssets.Scripts.LevelLogic
{
    public class Character : MonoBehaviour, ICharacter, ICharacterTypeOwner
    {
        [SerializeField, ReadOnly] private PeopleType _peopleType;
        [SerializeField, ReadOnly] private MovementType _movementType;
        private ModelPeopleMovement _modelPeopleMovement;
        private bool isCharacterActive;
        private NavMeshAgent _navMeshAgent;
        private Animator _animator;
        public Transform Transform => transform;
        public PeopleType PeopleType => _peopleType;
        public event Action OnPeopleTypeChanged;
        public event Action OnPeopleZoneChanged;


        public void Init(PeopleType type, MovementType dataMovementType)
        {
            _peopleType = type;
            _movementType = dataMovementType;
            OnPeopleTypeChanged?.Invoke();
        }

        public void OnChangeState(bool isActive)
        {
            isCharacterActive = isActive;
        }

        private void OnMouseDown()
        {
            isCharacterActive = true;
        }
    }
}