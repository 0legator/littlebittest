﻿using System;
using DG.Tweening;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Services;
using UnityEngine;

namespace InternalAssets.Scripts.LevelLogic
{
    public class ColorChanger : MonoBehaviour
    {
        [SerializeField] private Renderer mainMat;
        private ICharacterTypeOwner _characterTypeOwner;
        private Material tempMat;
        private Sequence tween;
        private void Awake()
        {
            _characterTypeOwner = GetComponent<ICharacterTypeOwner>();
            _characterTypeOwner.OnPeopleTypeChanged += ChangeMatColor;
        }

        private void Start()
        {
            SetPulse(this, false);
            tempMat = mainMat.material;
            AllServices.Container.Single<IZoneSystemService>().OnUpdateZone += SetPulse;
        }

        private void OnDestroy()
        {
            _characterTypeOwner.OnPeopleTypeChanged -= ChangeMatColor;
            AllServices.Container.Single<IZoneSystemService>().OnUpdateZone -= SetPulse;
        }

        private void ChangeMatColor()
        {
            switch (_characterTypeOwner.PeopleType)
            {
                case PeopleType.None:
                    throw new ArgumentOutOfRangeException();
                    break;
                case PeopleType.Blue:
                    mainMat.material.SetColor("MainColor", Color.blue);
                    break;
                case PeopleType.Red:
                    mainMat.material.SetColor("MainColor", Color.red);
                    break;
                case PeopleType.Green:
                    mainMat.material.SetColor("MainColor", Color.green);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetPulse(ColorChanger characterTypeOwner, bool isTrue)
        {
            if (characterTypeOwner == this && isTrue)
            {
                tween.Kill();
                mainMat.material.SetColor("PowerColor", Color.white);
                DOVirtual.Float(0, 1, 0.5f, (value) =>
                {
                    mainMat.material.SetFloat("PowerBlink", value);
                }).OnComplete(() =>
                {
                    DOVirtual.Float(1, 0, 0.5f, (value) =>
                    {
                        mainMat.material.SetFloat("PowerBlink", value);
                    });
                });
            }
            else if (characterTypeOwner == this && !isTrue)
            {
                tween.Kill();
                tween = DOTween.Sequence();
                mainMat.material.SetColor("PowerColor", Color.red);
                tween.Append(DOVirtual.Float(0, 1, 0.3f, (value) =>
                {
                    mainMat.material.SetFloat("PowerBlink", value);
                }));
                tween.Append(DOVirtual.Float(1, 0, 0.3f, (value) =>
                {
                    mainMat.material.SetFloat("PowerBlink", value);
                }));
                tween.SetLoops(-1, LoopType.Restart);
            }
        }
    }
}