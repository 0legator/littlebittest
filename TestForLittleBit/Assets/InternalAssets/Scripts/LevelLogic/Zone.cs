﻿using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Services;
using UnityEngine;

namespace InternalAssets.Scripts.LevelLogic
{
    public class Zone : MonoBehaviour
    {
        [SerializeField] private PeopleType zoneType;
        private BoxCollider _boxCollider;
        public PeopleType ZoneType => zoneType;
        public Bounds Bounds => _boxCollider.bounds;
        
        private void Awake()
        {
            _boxCollider = GetComponentInChildren<BoxCollider>();
        }
        
        private void OnTriggerEnter(Collider other)
        {
            AllServices.Container.Single<IZoneSystemService>().OnEnter(other, this);
        }

        private void OnTriggerExit(Collider other)
        {
            AllServices.Container.Single<IZoneSystemService>().OnExit(other, this);
        }
    }
}