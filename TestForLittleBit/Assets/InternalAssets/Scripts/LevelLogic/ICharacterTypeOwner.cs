﻿using System;
using InternalAssets.Scripts.Helpers;

namespace InternalAssets.Scripts.LevelLogic
{
    public interface ICharacterTypeOwner
    {
        public PeopleType PeopleType { get; }
        public event Action OnPeopleTypeChanged;
    }
}