﻿using System;
using InternalAssets.Scripts.Interfaces;
using InternalAssets.Scripts.Services;
using UnityEngine;
using UnityEngine.AI;

namespace InternalAssets.Scripts.LevelLogic
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavMeshMovable : MonoBehaviour, IMovableToPoint
    {
        private NavMeshAgent _navMeshAgent;
        public Vector3 Velocity => _navMeshAgent.velocity;
        private void Awake()
        {
            _navMeshAgent = GetComponent<NavMeshAgent>();
            _navMeshAgent.enabled = true;
        }
        
        public void MoveTo(Vector3 at)
        {
            _navMeshAgent.SetDestination(at);
        }
    }
}