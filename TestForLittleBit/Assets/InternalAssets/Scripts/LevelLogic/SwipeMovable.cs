﻿using System;
using InternalAssets.Scripts.Interfaces;
using InternalAssets.Scripts.Services;
using UnityEngine;
using UnityEngine.AI;

namespace InternalAssets.Scripts.LevelLogic
{
    [RequireComponent(typeof(Rigidbody))]
    public class SwipeMovable : MonoBehaviour, IMovableSwipe
    {
        private Rigidbody _rigidbody;
        private const float Force = 100;
        public Vector3 Velocity => _rigidbody.velocity;
        
        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        public void MoveTo(Vector3 at)
        {
            _rigidbody.AddForce(at * Force, ForceMode.Acceleration);
        }
    }
}