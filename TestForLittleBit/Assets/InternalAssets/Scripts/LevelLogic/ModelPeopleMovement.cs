﻿using System;
using InternalAssets.Scripts.Interfaces;
using UnityEngine;
using UnityEngine.AI;

namespace InternalAssets.Scripts.LevelLogic
{
    public class ModelPeopleMovement : IModelCharacterMovement
    {
        private readonly ICharacter _character;
        public event Action<bool> OnEnableNavMesh;
        public ModelPeopleMovement(ICharacter character)
        {
            _character = character;
            OnEnableNavMesh?.Invoke(true);
        }

        public void MoveTo(Vector3 at)
        {
            
        }
    }

    public interface IModelCharacterMovement
    {
        public event Action<bool> OnEnableNavMesh; 
        public void MoveTo(Vector3 at);
    }
}