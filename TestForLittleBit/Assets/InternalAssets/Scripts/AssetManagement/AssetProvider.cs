using UnityEngine;

namespace InternalAssets.Scripts.AssetManagement
{
    public class AssetProvider : IAssetProvider
    {
        public  AssetProvider()
        {
            
        }

        public GameObject Instantiate(string path, Vector3 at)
        {
            var prefab = Resources.Load<GameObject>(path);
            return Object.Instantiate(prefab, at, Quaternion.identity);
        }
        
        public TBehavior LoadPrefab<TBehavior>(string path) where TBehavior : Object
        {
            var prefab = Resources.Load<TBehavior>(path);
            return prefab;
        }

        public GameObject Instantiate(string path)
        {
            var prefab = Resources.Load<GameObject>(path);
            return Object.Instantiate(prefab);
        }
    }
}