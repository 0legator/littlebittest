namespace InternalAssets.Scripts.AssetManagement
{
    public static class AssetPath
    {
        public const string BootStarterPath = "BootStarter";
        public const string LevelsSettingsPath = "LevelsSettings";
        public const string InputSystemPath = "InputSystem";
        public const string LayerMaskPath = "LayerMask";
        public const string WinUIPath = "UI/WinUI";
        public const string GameUIPath = "UI/GameUI";
        public const string CameraSystemPath = "CameraSystem";
    }
}