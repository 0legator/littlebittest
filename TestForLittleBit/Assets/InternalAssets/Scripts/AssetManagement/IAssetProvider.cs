using InternalAssets.Scripts.Services;
using UnityEngine;

namespace InternalAssets.Scripts.AssetManagement
{
    public interface IAssetProvider : IService
    {
        GameObject Instantiate(string path, Vector3 at);
        GameObject Instantiate(string path);
        TBehavior LoadPrefab<TBehavior>(string path) where TBehavior : Object;
    }
}