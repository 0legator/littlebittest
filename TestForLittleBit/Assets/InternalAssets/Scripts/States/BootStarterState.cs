﻿using System;
using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.GameLoader;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Services;
using UnityEngine;

namespace InternalAssets.Scripts.States
{
    public class BootStarterState : IState
    {
        private readonly GameStateMachine _gameStateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly AllServices _allServices;
        private IAssetProvider GetAssetProvider() => _allServices.Single<IAssetProvider>();
        
        public BootStarterState(GameStateMachine gameStateMachine, SceneLoader sceneLoader, AllServices allServices)
        {
            _gameStateMachine = gameStateMachine;
            _sceneLoader = sceneLoader;
            _allServices = allServices;
        }

        public void Enter()
        {
            RegisterServices();
            _sceneLoader.LoadScene(StaticData.BootSceneName, OnEnterLevel, ProgressUI);
        }

        private void OnEnterLevel()
        {
            Debug.Log("Loaded!");
            _gameStateMachine.Enter<LoadLevelCharacterState>();
        }

        private void ProgressUI(float value)
        {
            //update slider value
        }
        
        public void Exit()
        {
            
        }

        private void RegisterServices()
        {
            _allServices.RegisterSingle<IAssetProvider>(new AssetProvider());
            _allServices.RegisterSingle<ILevelInfoService>(new LevelInfoService(GetAssetProvider()));
        }
    }
}