﻿
using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.GameLoader;
using InternalAssets.Scripts.Services;
using IState = InternalAssets.Scripts.Helpers.IState;

namespace InternalAssets.Scripts.States
{
    public class EndLevelState : IState
    {
        private readonly GameStateMachine _gameStateMachine;
        private readonly AllServices _allServices;

        public EndLevelState(GameStateMachine gameStateMachine, AllServices allServices)
        {
            _gameStateMachine = gameStateMachine;
            _allServices = allServices;
        }

        public void Enter()
        {
            CreateUI();
        }

        public void Exit()
        {
            
        }

        private void CreateUI()
        {
            _allServices.Single<ISystemUIService>().ShowFinishUI(LoadNextLevel);
        }

        private void LoadNextLevel()
        {
            _gameStateMachine.Enter<LoadLevelCharacterState>();
        }
    }
}