﻿using System;
using System.Collections.Generic;
using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.GameLoader;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.LevelLogic;
using InternalAssets.Scripts.Services;

namespace InternalAssets.Scripts.States
{
    public class LoadLevelCharacterState : IState
    {
        private readonly GameStateMachine _gameStateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly AllServices _allServices;

        public LoadLevelCharacterState(GameStateMachine gameStateMachine, SceneLoader sceneLoader, AllServices allServices)
        {
            _gameStateMachine = gameStateMachine;
            _sceneLoader = sceneLoader;
            _allServices = allServices;
        }

        public void Enter()
        {
            _sceneLoader.LoadScene(StaticData.PeopleLevelName, OnLoad, OnUpdate);
            //Показать UI загрузки
        }

        public void Exit()
        {
            //Скрыть UI загрузки

        }

        private void OnLoad()
        {
            _allServices.RegisterSingle<IUIFactory>(new UIFactoryService(_allServices.Single<IAssetProvider>()));
            _allServices.RegisterSingle<IFindZoneService>(new FindZoneService());
            _allServices.RegisterSingle<ISystemUIService>(new SystemUIService(_allServices.Single<IUIFactory>()));
            var zones = _allServices.Single<IFindZoneService>().GetZones();
            _allServices.RegisterSingle<IGameObjectFactory>(new GameObjectFactory(
                _allServices.Single<IAssetProvider>(),
                _allServices.Single<ILevelInfoService>(),
                zones));
            _gameStateMachine.Enter<GameLoopLoadLevelCharacter>();
            _allServices.Single<ISystemUIService>().ShowGameUI(_allServices.Single<ILevelInfoService>(), _allServices.Single<IZoneSystemService>());
        }

        private void OnUpdate(float f)
        {
            
        }
    }
}