﻿using System;
using InternalAssets.Scripts.AssetManagement;
using InternalAssets.Scripts.GameLoader;
using InternalAssets.Scripts.Services;
using Unity.VisualScripting;
using UnityEngine;
using IState = InternalAssets.Scripts.Helpers.IState;

namespace InternalAssets.Scripts.States
{
    public class GameLoopLoadLevelCharacter : IState
    {
        private readonly GameStateMachine _gameStateMachine;
        private readonly SceneLoader _sceneLoader;
        private readonly AllServices _allServices;
        private ICameraSystemService _cameraSystem;
        private IInputSystem _inputSystem;
        public GameLoopLoadLevelCharacter(GameStateMachine gameStateMachine, SceneLoader sceneLoader,
            AllServices allServices)
        {
            _gameStateMachine = gameStateMachine;
            _sceneLoader = sceneLoader;
            _allServices = allServices;
        }

        public void Enter()
        {
            var characters = _allServices.Single<IGameObjectFactory>().CreateCharacter();
            _inputSystem = _allServices.Single<IGameObjectFactory>().CreateInputSystem();
            _allServices.RegisterSingle<ISelectableService>(new SelectableService(characters, _inputSystem));
            _allServices.RegisterSingle<IInputSystem>(_inputSystem);
            _cameraSystem = _allServices.Single<IGameObjectFactory>().CreateCameraSystem();
            _allServices.RegisterSingle<ICameraSystemService>(_cameraSystem);
            _cameraSystem.Construct(_allServices.Single<ISelectableService>(), _allServices.Single<IInputSystem>());
            _allServices.Single<IInputSystem>().Construct(characters, _cameraSystem.GetMainCamera());
            var zones = _allServices.Single<IFindZoneService>().GetZones();
            _allServices.RegisterSingle<IZoneSystemService>(new ZoneSystemService(zones,
                _allServices.Single<ILevelInfoService>(), FinishLevel));
        }

        private void FinishLevel()
        {
            _gameStateMachine.Enter<EndLevelState>();
        }
        
        public void Exit()
        {
            _inputSystem.DisableService();
        }
    }
}