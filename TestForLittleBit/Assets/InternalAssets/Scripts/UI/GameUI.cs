﻿using System;
using DG.Tweening;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.Services;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace InternalAssets.Scripts.UI
{
    public class GameUI : MonoBehaviour, IElementUI
    {
        [SerializeField] private Slider progressBar;
        [SerializeField] private TextMeshProUGUI currentLevel;
        [SerializeField] private TextMeshProUGUI nextLevel;

        private ZoneSystemService _zoneSystemService;

        public void Enable()
        {
            gameObject.SetActive(true);
            currentLevel.SetText(PlayerPrefsController.LevelForAnalytics.ToString());
            nextLevel.SetText((PlayerPrefsController.LevelForAnalytics + 1).ToString());
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public MonoBehaviour ReturnMonoBehaviour()
        {
            return this;
        }

        public void UpdateProgressBar(IZoneSystemService zoneSystemService)
        {
            progressBar.DOValue(zoneSystemService.ReturnProgress(), 0.5f);
        }
    }
}