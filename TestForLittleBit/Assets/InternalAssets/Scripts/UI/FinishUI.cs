﻿using System;
using InternalAssets.Scripts.Helpers;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace InternalAssets.Scripts.UI
{
    public class FinishUI : MonoBehaviour, IElementInteractiveUI
    {
        [SerializeField] private Button finishButton;
        public event Action OnClick;

        private void Awake()
        {
            finishButton.onClick.AddListener(() =>
            {
                PlayerPrefsController.Level++;
                Wait.ForSeconds(0.5f, () =>
                {
                    OnClick?.Invoke();
                });
            });
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        public MonoBehaviour ReturnMonoBehaviour()
        {
            return this;
        }
    }
}