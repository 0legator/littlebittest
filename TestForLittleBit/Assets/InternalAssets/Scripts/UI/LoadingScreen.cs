﻿using System;
using DG.Tweening;
using UnityEngine;

namespace InternalAssets.Scripts.UI
{
    [RequireComponent(typeof(CanvasGroup))]
    public class LoadingScreen : MonoBehaviour
    {
        private CanvasGroup _canvasGroup;

        private void Awake()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public void Show()
        {
            DOVirtual.Float(0, 1, 0f, value =>
            {
                _canvasGroup.alpha = value;
            });
        }

        public void Hide()
        {
            DOVirtual.Float(1, 0, 0, value =>
            {
                _canvasGroup.alpha = value;
            });
        }
    }
}