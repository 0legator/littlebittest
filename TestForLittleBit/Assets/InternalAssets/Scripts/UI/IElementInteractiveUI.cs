﻿using System;

namespace InternalAssets.Scripts.UI
{
    public interface IElementInteractiveUI : IElementUI
    {
        public event Action OnClick;
    }
}