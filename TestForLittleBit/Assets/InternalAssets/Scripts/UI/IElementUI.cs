﻿using System;
using UnityEngine;

namespace InternalAssets.Scripts.UI
{
    public interface IElementUI
    {
        public void Enable();
        public void Disable();
        public MonoBehaviour ReturnMonoBehaviour();
    }
}