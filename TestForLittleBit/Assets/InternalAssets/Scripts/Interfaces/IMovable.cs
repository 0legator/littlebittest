﻿using UnityEngine;

namespace InternalAssets.Scripts.Interfaces
{
    public interface IMovable
    {
        public Vector3 Velocity { get; }
        public void MoveTo(Vector3 at);
        
    }
}