﻿namespace InternalAssets.Scripts.Helpers
{
    public interface IState
    {
        public void Enter();
        public void Exit();
    }
}