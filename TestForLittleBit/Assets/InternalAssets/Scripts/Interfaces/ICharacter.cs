﻿using InternalAssets.Scripts.Helpers;
using UnityEngine;

namespace InternalAssets.Scripts.Interfaces
{
    public interface ICharacter
    {
        Transform Transform { get; }
        public void Init(PeopleType peopleType, MovementType dataMovementType);
        void OnChangeState(bool isActive);
    }
}