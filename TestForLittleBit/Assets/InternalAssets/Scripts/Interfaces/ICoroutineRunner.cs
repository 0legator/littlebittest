﻿using System.Collections;
using UnityEngine;

namespace InternalAssets.Scripts.Helpers
{
    public interface ICoroutineRunner
    {
        Coroutine StartCoroutine(IEnumerator enumerator);
    }
}