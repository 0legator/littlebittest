﻿using System;
using System.Collections.Generic;
using InternalAssets.Scripts.Helpers;
using InternalAssets.Scripts.LevelLogic;
using UnityEngine;

namespace InternalAssets.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Level", menuName = "Setting/Level Setting", order = 0)]
    public class LevelSettingsSO : ScriptableObject
    {
        [SerializeField] private List<CharacterData> characterDatas = new List<CharacterData>();

        public List<CharacterData> CharacterDatas => characterDatas;
    }

    [Serializable]
    public class CharacterData
    {
        [SerializeField] private int nums;
        [SerializeField] private PeopleType type;
        [SerializeField] private MovementType movementType;
        [SerializeField] private Character character;

        public int Nums => nums;
        public PeopleType Type => type;
        public MovementType MovementType => movementType;
        public Character Character => character;
    }
}