﻿using System.Collections.Generic;
using UnityEngine;

namespace InternalAssets.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Levels", menuName = "Setting/Levels", order = 0)]
    public class LevelsSO : ScriptableObject
    {
        [SerializeField] List<LevelSettingsSO> levels = new List<LevelSettingsSO>();

        public List<LevelSettingsSO> Levels => levels;
    }
}