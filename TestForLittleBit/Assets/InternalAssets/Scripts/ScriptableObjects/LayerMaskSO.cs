﻿using UnityEngine;

namespace InternalAssets.Scripts.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Layer mask", menuName = "Setting/Layer Mask", order = 0)]
    public class LayerMaskSO : ScriptableObject
    {
        [SerializeField] private LayerMask layerMask;

        public LayerMask LayerMask => layerMask;
    }
}